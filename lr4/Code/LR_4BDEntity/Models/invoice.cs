//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LR_4BDEntity.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public invoice()
        {
            this.sales_invoice = new HashSet<sales_invoice>();
        }
    
        public int invoice_id { get; set; }
        public int raw_materials_id { get; set; }
        public int qty { get; set; }
        public System.DateTime date { get; set; }
        public int contract_id { get; set; }
    
        public virtual contract contract { get; set; }
        public virtual raw_materials raw_materials { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sales_invoice> sales_invoice { get; set; }
    }
}
