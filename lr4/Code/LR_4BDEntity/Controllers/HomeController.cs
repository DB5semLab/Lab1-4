﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Diagnostics;
using LR_4BDEntity.Models;

namespace LR_4BDEntity.Controllers
{
    public class query4result
    {
        public int expenses { get; set; }
        public string month { get; set; }
    }
    public class HomeController : Controller
    {
        stockEntities db = new stockEntities();

        public ActionResult Index()
        {

            IEnumerable<vendors> Vendors = db.vendors;
           // Debug.WriteLine();
            ViewBag.Vendors = Vendors;
            var lrdb = new Entities1();
            //1.	Определить изделие, в которое входит больше всего материалов типа 'цветной металл'. 
            ViewBag.query1 = lrdb.product.SqlQuery("query1").ToList();
            ViewBag.query2 = lrdb.product.SqlQuery("query2").ToList();
            ViewBag.query3 = lrdb.product.SqlQuery("query3").ToList();
            ViewBag.query4 = lrdb.Database.SqlQuery<query4result>("query4").ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult AddVendor()
        {
            return View("EditVendor");
        }
        
        [HttpPost]
        public ActionResult AddVendor(vendors a)
        {
            a.vendor_id++;
            foreach (var v in db.vendors)
            {
                if (a.vendor_id == v.vendor_id)
                {
                    a.vendor_id++;
                }
            }
            db.vendors.Add(a);
            db.SaveChanges();
            ViewBag.Operation = "додано";
            return View("EditSuccess");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            vendors a = db.vendors.Find(id);
            if (FormMethod.Get == 0)
            {
                ViewBag.vendors = a;
                return View("DeleteVendor", a);
            }
        }

        [HttpPost]
        public ActionResult Delete(vendors a)
        {
            db.vendors.Remove(db.vendors.Find(Int32.Parse(RouteData.Values["id"].ToString())));
            db.SaveChanges();
            IEnumerable<vendors> Vendors = db.vendors;
            ViewBag.vendors = Vendors;
            return View("Index");
        }

        [HttpGet]
        public ActionResult EditVendor(int? id)
        {
            if (id == null) RedirectToAction("Index");
            vendors a = db.vendors.Find(id);
           
            return View(a);
        }

        [HttpPost]
        public ActionResult EditVendor(vendors a)
        {
            db.Entry(a).State = EntityState.Modified;
            db.SaveChanges();
            ViewBag.Operation = "Відредаговано";
            return View("EditSuccess");
        }

        //[HttpGet]
        //public ActionResult EditVendor(int vendor_id)
        //{
        //    return View();
        //}
    }
}