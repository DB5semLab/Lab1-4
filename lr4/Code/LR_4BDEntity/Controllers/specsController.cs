﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LR_4BDEntity.Models;

namespace LR_4BDEntity.Controllers
{
    public class specsController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: specs
        public ActionResult Index()
        {
            var specs = db.specs.Include(s => s.material).Include(s => s.product);
            return View(specs.ToList());
        }

        // GET: specs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            specs specs = db.specs.Find(id);
            if (specs == null)
            {
                return HttpNotFound();
            }
            return View(specs);
        }

        // GET: specs/Create
        public ActionResult Create()
        {
            ViewBag.material_id = new SelectList(db.material, "material_id", "type");
            ViewBag.product_id = new SelectList(db.product, "product_id", "code");
            return View();
        }

        // POST: specs/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "spec_id,creation_date,cancel_date,product_id,material_id")] specs specs)
        {
            if (ModelState.IsValid)
            {
                db.specs.Add(specs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.material_id = new SelectList(db.material, "material_id", "type", specs.material_id);
            ViewBag.product_id = new SelectList(db.product, "product_id", "code", specs.product_id);
            return View(specs);
        }

        // GET: specs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            specs specs = db.specs.Find(id);
            if (specs == null)
            {
                return HttpNotFound();
            }
            ViewBag.material_id = new SelectList(db.material, "material_id", "type", specs.material_id);
            ViewBag.product_id = new SelectList(db.product, "product_id", "code", specs.product_id);
            return View(specs);
        }

        // POST: specs/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "spec_id,creation_date,cancel_date,product_id,material_id")] specs specs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.material_id = new SelectList(db.material, "material_id", "type", specs.material_id);
            ViewBag.product_id = new SelectList(db.product, "product_id", "code", specs.product_id);
            return View(specs);
        }

        // GET: specs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            specs specs = db.specs.Find(id);
            if (specs == null)
            {
                return HttpNotFound();
            }
            return View(specs);
        }

        // POST: specs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            specs specs = db.specs.Find(id);
            db.specs.Remove(specs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
