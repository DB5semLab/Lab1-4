﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lr3.Forms;

namespace Lr3
{
    public partial class MainForm : Form
    {
        protected RawMaterials RawMaterialsForm;
        protected VendorsSearch VendorsSearchForm;
        protected VendorContract VendorContractForm;
        protected Order OrderForm;
        protected Vendors VendorsForm;
        protected Procedures ProceduresForm;

        public MainForm()
        {
            InitializeComponent();
            this.RawMaterialsForm = new Forms.RawMaterials();
            this.VendorsSearchForm = new Forms.VendorsSearch();
            this.VendorContractForm = new Forms.VendorContract();
            this.OrderForm = new Forms.Order();
            this.VendorsForm = new Forms.Vendors();
            this.ProceduresForm = new Forms.Procedures();
        }

        protected bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form form in fc)
            {
                if (form.Name == name)
                {
                    return true;
                }
            }
            return false;
        }

        protected void ShowForm(Form formToShow)
        {
            if (this.CheckOpened(formToShow.Name))
            {
                formToShow.Focus();
            }
            else
            {
                formToShow = (Form)Activator.CreateInstance(formToShow.GetType()); ;
                formToShow.Show();
            }
        }

        private void матеріалиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowForm(this.RawMaterialsForm);
        }

        private void vendorsSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowForm(this.VendorsSearchForm);
        }


        private void vendorContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowForm(this.VendorContractForm); 
        }

        private void замовленняToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowForm(this.OrderForm);
        }

        private void простоПостачальникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowForm(this.VendorsForm);
        }

        private void процедуриToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowForm(ProceduresForm);
        }
    }
}
