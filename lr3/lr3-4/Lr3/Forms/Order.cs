﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lr3.Forms
{
    public partial class Order : Form
    {
        public Order()
        {
            InitializeComponent();
        }

        private void orderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.orderBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.stockDataSet);

        }

        private void Order_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "stockDataSet.contract". При необходимости она может быть перемещена или удалена.
            this.contractTableAdapter.Fill(this.stockDataSet.contract);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "stockDataSet.order". При необходимости она может быть перемещена или удалена.
            this.orderTableAdapter.Fill(this.stockDataSet.order);
            this.raw_materialsTableAdapter1.Fill(this.stockDataSet.raw_materials);
        }
    }
}
