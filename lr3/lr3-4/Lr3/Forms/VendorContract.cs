﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lr3.Forms
{
    public partial class VendorContract : Form
    {
        public VendorContract()
        {
            InitializeComponent();
        }

        private void VendorContract_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "stockDataSet.vendors". При необходимости она может быть перемещена или удалена.
            this.vendorsTableAdapter.Fill(this.stockDataSet.vendors);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "stockDataSet.contract". При необходимости она может быть перемещена или удалена.
            this.contractTableAdapter.Fill(this.stockDataSet.contract);

        }
    }
}
