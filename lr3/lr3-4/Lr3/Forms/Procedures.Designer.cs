﻿namespace Lr3.Forms
{
    partial class Procedures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
            this.sqlDataAdapter1 = new System.Data.SqlClient.SqlDataAdapter();
            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
            this.sqlCommand1 = new System.Data.SqlClient.SqlCommand();
            this.shopDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stockDataSet = new Lr3.stockDataSet();
            this.executeButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.returnValueProcButton = new System.Windows.Forms.Button();
            this.updateProcButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.shopDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // sqlConnection1
            // 
            this.sqlConnection1.ConnectionString = "Data Source=DESKTOP-O13KO67;Initial Catalog=stock;Integrated Security=True";
            this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
            // 
            // sqlCommand1
            // 
            this.sqlCommand1.CommandText = "stock.proc1";
            this.sqlCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlCommand1.Connection = this.sqlConnection1;
            // 
            // shopDataSetBindingSource
            // 
            this.shopDataSetBindingSource.DataSource = this.stockDataSet;
            this.shopDataSetBindingSource.Position = 0;
            // 
            // stockDataSet
            // 
            this.stockDataSet.DataSetName = "stockDataSet";
            this.stockDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(13, 13);
            this.executeButton.Margin = new System.Windows.Forms.Padding(4);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(126, 28);
            this.executeButton.TabIndex = 2;
            this.executeButton.Text = "selectProc";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 76);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(445, 257);
            this.dataGridView1.TabIndex = 3;
            // 
            // returnValueProcButton
            // 
            this.returnValueProcButton.Location = new System.Drawing.Point(162, 13);
            this.returnValueProcButton.Margin = new System.Windows.Forms.Padding(4);
            this.returnValueProcButton.Name = "returnValueProcButton";
            this.returnValueProcButton.Size = new System.Drawing.Size(146, 28);
            this.returnValueProcButton.TabIndex = 4;
            this.returnValueProcButton.Text = "returnValueProc";
            this.returnValueProcButton.UseVisualStyleBackColor = true;
            this.returnValueProcButton.Click += new System.EventHandler(this.returnValueProcButton_Click);
            // 
            // updateProcButton
            // 
            this.updateProcButton.Location = new System.Drawing.Point(457, 24);
            this.updateProcButton.Margin = new System.Windows.Forms.Padding(4);
            this.updateProcButton.Name = "updateProcButton";
            this.updateProcButton.Size = new System.Drawing.Size(100, 28);
            this.updateProcButton.TabIndex = 5;
            this.updateProcButton.Text = "updateProc";
            this.updateProcButton.UseVisualStyleBackColor = true;
            this.updateProcButton.Click += new System.EventHandler(this.updateProcButton_Click);
            // 
            // Procedures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 368);
            this.Controls.Add(this.updateProcButton);
            this.Controls.Add(this.returnValueProcButton);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.executeButton);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Procedures";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.shopDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private System.Data.SqlClient.SqlConnection sqlConnection1;
        private System.Data.SqlClient.SqlCommand sqlInsertCommand1;
        private System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
        private System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
        private System.Data.SqlClient.SqlDataAdapter sqlDataAdapter1;
        private System.Data.SqlClient.SqlCommand sqlCommand1;
        private System.Windows.Forms.BindingSource shopDataSetBindingSource;
        private stockDataSet stockDataSet;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button returnValueProcButton;
        private System.Windows.Forms.Button updateProcButton;
    }
}