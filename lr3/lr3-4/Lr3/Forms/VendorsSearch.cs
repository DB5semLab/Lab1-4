﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lr3.Forms
{
    public partial class VendorsSearch : Form
    {
        public VendorsSearch()
        {
            InitializeComponent();

            this.searchFailureLabel.Visible = false;
            this.switchResultsVisibility(false);
        }

        protected void switchResultsVisibility(Boolean visibility)
        {
            this.searchResultIdLabel.Visible = this.searchResultNameLabel.Visible = visibility;
        }


        private void fillByNameToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                int itemsFound = this.vendorsTableAdapter1.FillByName(this.stockDataSet1.vendors, nameToolStripTextBox.Text);
                Boolean isFound = Convert.ToBoolean(itemsFound);
                this.switchResultsVisibility(isFound);
                this.searchFailureLabel.Visible = !isFound;
            }
            catch (System.Exception ex)
            {
                this.switchResultsVisibility(false);
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
    }
}
