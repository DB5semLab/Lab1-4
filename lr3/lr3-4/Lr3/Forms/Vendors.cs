﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Lr3.Forms
{
    public partial class Vendors : Form
    {
        public Vendors()
        {
            InitializeComponent();
        }

        private void Vendors_Load(object sender, EventArgs e)
        {
            this.vendorsTableAdapter.Fill(this.stockDataSet.vendors);

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            vendorsTableAdapter.Update(stockDataSet.vendors);
            MessageBox.Show("Saved");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int i = this.vendorsBindingSource.Find("name", textBox1.Text);
            if (i == -1)
            {
                DataView dv = new DataView(stockDataSet.vendors as DataTable);
                dv.RowFilter = string.Format("name LIKE '{0}*'", textBox1.Text);
                if (dv.Count != 0)
                {
                    i = vendorsBindingSource.Find("name", dv[0]["name"]);
                }
                dv.Dispose();
                vendorsBindingSource.Position = i;
            }
            vendorsBindingSource.Position = i;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            vendorsBindingSource.Filter = "name='" + textBox2.Text + "'";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            vendorsBindingSource.Filter = "";
            textBox2.Clear();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fn = saveFileDialog1.FileName;
                StreamWriter sw = new StreamWriter(fn, false, System.Text.Encoding.GetEncoding(1251));
                DataView dv = new DataView(stockDataSet.vendors as DataTable);
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<Vendor List>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<h1>VendorList</h1>");
                sw.WriteLine("<table border=1>");
                sw.WriteLine("<tr>");
                sw.WriteLine("<th<h3>Name</h3></th>");
                sw.WriteLine("</tr>");
                for (int i = 0; i < dv.Count; i++)
                {
                    sw.WriteLine("<tr>");
                    sw.WriteLine("<td>" + dv[i]["name"] + "<td>");
                    sw.WriteLine("</tr>");
                }
                sw.WriteLine("</table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
           /* Form2 form2 = new Form2();
            form2.Show();*/
        }
    }
}
