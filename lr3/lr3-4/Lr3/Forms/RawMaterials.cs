﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lr3.Forms
{
    public partial class RawMaterials : Form
    {
        public RawMaterials()
        {
            InitializeComponent();
        }

        private void RawMaterials_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "stockDataSet.raw_materials". При необходимости она может быть перемещена или удалена.
            this.raw_materialsTableAdapter.Fill(this.stockDataSet.raw_materials);

        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            this.UpdateData();
        }

        protected void UpdateData(string successMessage = "OK")
        {
            this.raw_materialsTableAdapter.Update(this.stockDataSet);
            MessageBox.Show(successMessage);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            uint counter = 0;
            foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
            {
                if (!oneCell.OwningRow.IsNewRow && oneCell.Selected)
                {
                    dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                    ++counter;
                }
            }

            if (counter > 0)
            {
                this.UpdateData(String.Format("Видалено записів: {0}", counter));
            }
        }
    }
}
