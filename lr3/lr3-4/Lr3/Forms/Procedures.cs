﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Lr3.Forms
{
    public partial class Procedures : Form
    {
        protected DataSet stockDataSetMysql;
        private MySqlConnection mySqlConnection;

        public Procedures()
        {
            InitializeComponent();

            mySqlConnection = new MySqlConnection("Server=localhost;Database=stock;Uid=root;Pwd=abcABC123;SslMode=none");
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            //var zzz = 
            //MessageBox.Show(zzz);
            //string query = "select * from vendors";
            //var adapter = new MySqlDataAdapter(query, mySqlConnection);
            //var dataTable = new DataTable();
            //adapter.Fill(dataTable);
            //dataGridView1.DataSource = dataTable;

            sqlConnection1.Open();
            sqlDataAdapter1.SelectCommand = sqlCommand1;
            sqlCommand1.ExecuteNonQuery();
            var dt = new DataTable();
            sqlDataAdapter1.Fill(dt);
            sqlConnection1.Close();
            dataGridView1.DataSource = dt;
        }

        private void returnValueProcButton_Click(object sender, EventArgs e)
        {
            mySqlConnection.Open();
            var proc = new MySqlCommand("return_random", mySqlConnection);
            proc.CommandType = CommandType.StoredProcedure;
            var adapter = new MySqlDataAdapter();
            adapter.SelectCommand = proc;
            proc.Parameters.Add("out_number", MySqlDbType.Int32, 3);
            proc.Parameters["out_number"].Direction = ParameterDirection.Output;
            proc.ExecuteNonQuery();
            MessageBox.Show(
                String.Format(
                    "out_value: {0}",
                    proc.Parameters["out_number"].Value.ToString()
                )
            );
            mySqlConnection.Close();
        }

        private void selectMaterials(string id = null)
        {
            mySqlConnection.OpenAsync();
            string query = "select * from raw_materials";
            if(id != null)
            {
                query += " where raw_materials_id = " + id;
            }
            var adapter = new MySqlDataAdapter(query, mySqlConnection);
            var dataTable = new DataTable();
            adapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
            mySqlConnection.CloseAsync();
        }

        private void updateProcButton_Click(object sender, EventArgs e)
        {
            this.selectMaterials();
            mySqlConnection.Open();
            var proc = new MySqlCommand("inc_materials_qty", mySqlConnection);
            proc.CommandType = CommandType.StoredProcedure;
            string id_to_update = Microsoft.VisualBasic.Interaction.InputBox("Enter raw_material id:", "updateProcWithParams", "1");
            proc.Parameters.Add("raw_materials_id", MySqlDbType.Int32);
            proc.Parameters["raw_materials_id"].Value = id_to_update;
            proc.Parameters["raw_materials_id"].Direction = ParameterDirection.Input;

            int UspeshnoeIzmenenie = proc.ExecuteNonQuery();

            if (UspeshnoeIzmenenie != 0)
            {
                MessageBox.Show("Изменения внесены", "Изменение записи");
                this.selectMaterials(id_to_update);
            }
            else
            {
                MessageBox.Show("Не удалось внести изменения", "Изменение записи");
            }
            mySqlConnection.Close();
        }
    }
}
