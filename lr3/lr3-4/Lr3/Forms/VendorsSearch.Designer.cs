﻿namespace Lr3.Forms
{
    partial class VendorsSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.searchFailureLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.searchResultIdLabel = new System.Windows.Forms.Label();
            this.vendorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stockDataSet1 = new Lr3.stockDataSet();
            this.fillByNameToolStrip = new System.Windows.Forms.ToolStrip();
            this.nameToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.nameToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.fillByNameToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.vendorsTableAdapter1 = new Lr3.stockDataSetTableAdapters.vendorsTableAdapter();
            this.searchResultNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.vendorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockDataSet1)).BeginInit();
            this.fillByNameToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchFailureLabel
            // 
            this.searchFailureLabel.AutoSize = true;
            this.searchFailureLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.searchFailureLabel.Location = new System.Drawing.Point(82, 117);
            this.searchFailureLabel.Name = "searchFailureLabel";
            this.searchFailureLabel.Size = new System.Drawing.Size(175, 20);
            this.searchFailureLabel.TabIndex = 7;
            this.searchFailureLabel.Text = "Нічого не знайдено";
            this.searchFailureLabel.Visible = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // searchResultIdLabel
            // 
            this.searchResultIdLabel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vendorsBindingSource, "vendor_id", true));
            this.searchResultIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.searchResultIdLabel.Location = new System.Drawing.Point(12, 106);
            this.searchResultIdLabel.Name = "searchResultIdLabel";
            this.searchResultIdLabel.Size = new System.Drawing.Size(305, 38);
            this.searchResultIdLabel.TabIndex = 8;
            this.searchResultIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.searchResultIdLabel.Visible = false;
            // 
            // vendorsBindingSource
            // 
            this.vendorsBindingSource.DataMember = "vendors";
            this.vendorsBindingSource.DataSource = this.stockDataSet1;
            // 
            // stockDataSet1
            // 
            this.stockDataSet1.DataSetName = "stockDataSet";
            this.stockDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fillByNameToolStrip
            // 
            this.fillByNameToolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.fillByNameToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nameToolStripLabel,
            this.nameToolStripTextBox,
            this.fillByNameToolStripButton});
            this.fillByNameToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.fillByNameToolStrip.Location = new System.Drawing.Point(0, 0);
            this.fillByNameToolStrip.Name = "fillByNameToolStrip";
            this.fillByNameToolStrip.Size = new System.Drawing.Size(329, 99);
            this.fillByNameToolStrip.TabIndex = 9;
            this.fillByNameToolStrip.Text = "fillByNameToolStrip";
            // 
            // nameToolStripLabel
            // 
            this.nameToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.nameToolStripLabel.Name = "nameToolStripLabel";
            this.nameToolStripLabel.Size = new System.Drawing.Size(327, 23);
            this.nameToolStripLabel.Text = "Назва постачальника:";
            // 
            // nameToolStripTextBox
            // 
            this.nameToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.nameToolStripTextBox.Name = "nameToolStripTextBox";
            this.nameToolStripTextBox.Size = new System.Drawing.Size(325, 30);
            // 
            // fillByNameToolStripButton
            // 
            this.fillByNameToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByNameToolStripButton.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.fillByNameToolStripButton.Name = "fillByNameToolStripButton";
            this.fillByNameToolStripButton.Size = new System.Drawing.Size(327, 29);
            this.fillByNameToolStripButton.Text = "Знайти";
            this.fillByNameToolStripButton.Click += new System.EventHandler(this.fillByNameToolStripButton_Click);
            // 
            // vendorsTableAdapter1
            // 
            this.vendorsTableAdapter1.ClearBeforeFill = true;
            // 
            // searchResultNameLabel
            // 
            this.searchResultNameLabel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vendorsBindingSource, "name", true));
            this.searchResultNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.searchResultNameLabel.Location = new System.Drawing.Point(12, 150);
            this.searchResultNameLabel.Name = "searchResultNameLabel";
            this.searchResultNameLabel.Size = new System.Drawing.Size(305, 48);
            this.searchResultNameLabel.TabIndex = 10;
            this.searchResultNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.searchResultNameLabel.Visible = false;
            // 
            // VendorsSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 207);
            this.Controls.Add(this.searchResultNameLabel);
            this.Controls.Add(this.fillByNameToolStrip);
            this.Controls.Add(this.searchResultIdLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchFailureLabel);
            this.Name = "VendorsSearch";
            this.Text = "VendorsSearch";
            ((System.ComponentModel.ISupportInitialize)(this.vendorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockDataSet1)).EndInit();
            this.fillByNameToolStrip.ResumeLayout(false);
            this.fillByNameToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label searchFailureLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label searchResultIdLabel;
        private System.Windows.Forms.BindingSource vendorsBindingSource;
        private stockDataSet stockDataSet1;
        private stockDataSetTableAdapters.vendorsTableAdapter vendorsTableAdapter1;
        private System.Windows.Forms.ToolStrip fillByNameToolStrip;
        private System.Windows.Forms.ToolStripLabel nameToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox nameToolStripTextBox;
        private System.Windows.Forms.ToolStripButton fillByNameToolStripButton;
        private System.Windows.Forms.Label searchResultNameLabel;
    }
}