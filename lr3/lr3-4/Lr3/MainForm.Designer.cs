﻿namespace Lr3
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.формиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.матеріалиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.постачальникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorContractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.простоПостачальникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.замовленняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.процедуриToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.формиToolStripMenuItem,
            this.процедуриToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(615, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // формиToolStripMenuItem
            // 
            this.формиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.матеріалиToolStripMenuItem,
            this.постачальникиToolStripMenuItem,
            this.замовленняToolStripMenuItem});
            this.формиToolStripMenuItem.Name = "формиToolStripMenuItem";
            this.формиToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.формиToolStripMenuItem.Text = "Форми";
            // 
            // матеріалиToolStripMenuItem
            // 
            this.матеріалиToolStripMenuItem.Name = "матеріалиToolStripMenuItem";
            this.матеріалиToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.матеріалиToolStripMenuItem.Text = "Матеріали";
            this.матеріалиToolStripMenuItem.Click += new System.EventHandler(this.матеріалиToolStripMenuItem_Click);
            // 
            // постачальникиToolStripMenuItem
            // 
            this.постачальникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsSearchToolStripMenuItem,
            this.vendorContractToolStripMenuItem,
            this.простоПостачальникиToolStripMenuItem});
            this.постачальникиToolStripMenuItem.Name = "постачальникиToolStripMenuItem";
            this.постачальникиToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.постачальникиToolStripMenuItem.Text = "Постачальники";
            // 
            // vendorsSearchToolStripMenuItem
            // 
            this.vendorsSearchToolStripMenuItem.Name = "vendorsSearchToolStripMenuItem";
            this.vendorsSearchToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
            this.vendorsSearchToolStripMenuItem.Text = "Пошук по назві";
            this.vendorsSearchToolStripMenuItem.Click += new System.EventHandler(this.vendorsSearchToolStripMenuItem_Click);
            // 
            // vendorContractToolStripMenuItem
            // 
            this.vendorContractToolStripMenuItem.Name = "vendorContractToolStripMenuItem";
            this.vendorContractToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
            this.vendorContractToolStripMenuItem.Text = "Постачальник - контракт";
            this.vendorContractToolStripMenuItem.Click += new System.EventHandler(this.vendorContractToolStripMenuItem_Click);
            // 
            // простоПостачальникиToolStripMenuItem
            // 
            this.простоПостачальникиToolStripMenuItem.Name = "простоПостачальникиToolStripMenuItem";
            this.простоПостачальникиToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
            this.простоПостачальникиToolStripMenuItem.Text = "Просто постачальники";
            this.простоПостачальникиToolStripMenuItem.Click += new System.EventHandler(this.простоПостачальникиToolStripMenuItem_Click);
            // 
            // замовленняToolStripMenuItem
            // 
            this.замовленняToolStripMenuItem.Name = "замовленняToolStripMenuItem";
            this.замовленняToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.замовленняToolStripMenuItem.Text = "Замовлення";
            this.замовленняToolStripMenuItem.Click += new System.EventHandler(this.замовленняToolStripMenuItem_Click);
            // 
            // процедуриToolStripMenuItem
            // 
            this.процедуриToolStripMenuItem.Name = "процедуриToolStripMenuItem";
            this.процедуриToolStripMenuItem.Size = new System.Drawing.Size(100, 24);
            this.процедуриToolStripMenuItem.Text = "Процедури";
            this.процедуриToolStripMenuItem.Click += new System.EventHandler(this.процедуриToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 453);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem формиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem матеріалиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem постачальникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorContractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem замовленняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem простоПостачальникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem процедуриToolStripMenuItem;
    }
}

