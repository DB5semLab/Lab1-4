CREATE USER IF NOT EXISTS rightless_user;
CREATE USER IF NOT EXISTS admin;

SET PASSWORD FOR admin = 'admin';
GRANT ALL PRIVILEGES on stock TO admin;

SET PASSWORD FOR rightless_user = 'user';

GRANT SELECT, UPDATE, DELETE on invoice TO rightless_user;
GRANT DELETE on raw_materials TO rightless_user;
GRANT INSERT on contract TO rightless_user;
GRANT SELECT, INSERT  on `order` TO rightless_user;
GRANT EXECUTE on PROCEDURE get_dep_mat_inc_by_period TO rightless_user;


