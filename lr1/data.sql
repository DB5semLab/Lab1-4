INSERT INTO schedule (schedule_id, description) VALUES (0, 'ранок'), (1, 'день');

INSERT INTO clients (client_id, name, sex, age, schedule_id) VALUES (0, 'Льоха', 'чол', 19, 1);
INSERT INTO clients (client_id, name, sex, age, schedule_id) VALUES (1, 'Антон', 'чол', 22, 1);
INSERT INTO clients (client_id, name, sex, age, schedule_id) VALUES (2, 'Ваньок', 'чол', 12, 1);
INSERT INTO clients (client_id, name, sex, age, schedule_id) VALUES (3, 'Ісус', 'чол', 33, 0);
INSERT INTO clients (client_id, name, sex, age, schedule_id) VALUES (4, 'Анджела', 'жін', 12, 0);

INSERT INTO trainers (trainer_id, name, sex, age) VALUES
  (0, 'Сірожа', 'чол', 40),
  (1, 'Ігор', 'чол', 19),
  (2, 'Арнольд', 'чол', 68);

INSERT INTO groups (group_id, type) VALUES (0, 'качки'), (1, 'фітнес');

INSERT INTO client_trainer (client_id, trainer_id) VALUES
  (0, 0),
  (1, 0),
  (2, 1),
  (3, 2),
  (4, 2);

INSERT INTO client_group (client_id, group_id) VALUES
  (0, 0),
  (1, 0),
  (2, 0),
  (3, 0),
  (4, 1);
