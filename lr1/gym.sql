/*trainers, clients, client_trainers, group, schedule*/
set FOREIGN_KEY_CHECKS=0;
DROP TABLE if EXISTS trainers;
CREATE TABLE trainers
(
  trainer_id  INT PRIMARY KEY,
  name        VARCHAR(50) NOT NULL,
  sex         VARCHAR(6),
  age         SMALLINT
);

DROP TABLE if EXISTS clients;
CREATE TABLE clients
(
  client_id INT PRIMARY KEY,
  name      VARCHAR(50) NOT NULL,
  sex       VARCHAR(6),
  age       SMALLINT,
schedule_id INT         NOT NULL REFERENCES schedule (schedule_id)
);

CREATE TABLE groups
(
  group_id INT PRIMARY KEY,
  type     VARCHAR(50) NOT NULL
);

CREATE TABLE schedule
(
  schedule_id INT PRIMARY KEY,
  description VARCHAR(60) NOT NULL
);

CREATE TABLE client_trainer
(
  client_id  INT NOT NULL,
  trainer_id INT NOT NULL,
  CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT trainer_id_fk FOREIGN KEY (trainer_id) REFERENCES trainers (trainer_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

drop TABLE if EXISTS client_group;
CREATE TABLE client_group
(
  client_id INT NOT NULL,
  group_id  INT NOT NULL,
  CONSTRAINT cg_client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT cg_group_id_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

ALTER TABLE client_group CONVERT TO CHARACTER SET utf8;
ALTER TABLE client_trainer CONVERT TO CHARACTER SET utf8;
ALTER TABLE clients CONVERT TO CHARACTER SET utf8;
ALTER TABLE groups CONVERT TO CHARACTER SET utf8;
ALTER TABLE schedule CONVERT TO CHARACTER SET utf8;
ALTER TABLE trainers CONVERT TO CHARACTER SET utf8;