#1
CREATE TABLE IF NOT EXISTS countAllRows (
  id         INT PRIMARY KEY AUTO_INCREMENT,
  table_name VARCHAR(50),
  rows_count INT
);

DROP PROCEDURE IF EXISTS countAllRows;

DELIMITER //
CREATE PROCEDURE countAllRows(IN tableName VARCHAR(50))
  BEGIN
    SET @var = 1;
    SET @count = CONCAT('SELECT COUNT(*) INTO @var from ', tableName);
    PREPARE stmt FROM @count;
    EXECUTE stmt;
    INSERT INTO countAllRows (table_name, rows_count) VALUES (tableName, @var);

  END//
DELIMITER ;

CALL countAllRows('clients');

# 2
DROP TABLE IF EXISTS countColumns_results;
CREATE TABLE IF NOT EXISTS countColumns_results (
  row_id    INT PRIMARY KEY AUTO_INCREMENT,
  tableName VARCHAR(50) NOT NULL,
  cnt       INT         NOT NULL
);


DELIMITER $$$
DROP PROCEDURE IF EXISTS countColumns;
CREATE PROCEDURE countColumns(IN tableName VARCHAR(50))
  BEGIN
    DECLARE cnt INT;

    SELECT count(*)
    FROM information_schema.columns
    WHERE table_name = tableName
    INTO cnt;

    INSERT INTO countColumns_results (tableName, cnt) VALUES
      (tableName, cnt);
  END;
$$$

CALL countColumns('clients');

# процедура, що визначає для кожного поля таблиці, кількість значень, що не повторюються

CREATE TABLE IF NOT EXISTS countUnique_results (
  id          INT PRIMARY KEY AUTO_INCREMENT,
  tableName   VARCHAR(50) NOT NULL,
  column_name VARCHAR(50) NOT NULL,
  cnt         INT         NOT NULL
);

DROP PROCEDURE IF EXISTS countUnique;
DELIMITER $$$

CREATE PROCEDURE countUnique(IN tableName VARCHAR(50))
  BEGIN
    DECLARE prevColName VARCHAR(50) DEFAULT '11111';
    DECLARE colName VARCHAR(50);
    DECLARE done INTEGER DEFAULT 0;
    DECLARE BankCursor CURSOR FOR (SELECT COLUMN_NAME
                                   FROM information_schema.columns
                                   WHERE table_name = tableName);

    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

    OPEN BankCursor;
    WHILE done = 0 DO
      FETCH BankCursor
      INTO colName;
      IF prevColName != colName
      THEN
        SET prevColName = colName;
        SET @count = CONCAT('SELECT DISTINCT count(', colName, ') INTO @cnt from ', tableName);
        PREPARE stmt FROM @count;
        EXECUTE stmt;
        INSERT INTO countUnique_results (tableName, column_name, cnt) VALUES (tableName, colName, @cnt);
      END IF;
    END WHILE;
    CLOSE BankCursor;

  END;
$$$
DELIMITER ;

CALL countUnique('clients');