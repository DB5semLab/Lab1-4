INSERT INTO groups (group_id, type) VALUES (3, 'нічна');
INSERT INTO schedule (schedule_id, description) VALUES (3, 'ніч');

# Тригер, що оновлює одночасно дані в двох таблицях
DROP TRIGGER IF EXISTS updateGroup;
DELIMITER HH1488
CREATE TRIGGER updateGroup
AFTER UPDATE ON clients
FOR EACH ROW
  BEGIN
    IF ((NEW.schedule_id != OLD.schedule_id) AND (NEW.schedule_id = 3))
    THEN
      UPDATE client_group cg
      SET cg.group_id = 3
      WHERE cg.client_id = NEW.client_id;
    END IF;
  END;
HH1488

UPDATE clients
SET schedule_id = 3
WHERE client_id = 4;

#Тригер, що знищує зв’язані дані одночасно в двох таблицях


INSERT IGNORE INTO schedule (schedule_id, description) VALUES (666, 'to_delete');
INSERT IGNORE INTO clients (client_id, name, sex, age, schedule_id) VALUES
  (666, 'zzz', 'zzz', 12, 666);

DROP TRIGGER IF EXISTS updateGroup;
DELIMITER HH1488
CREATE TRIGGER updateGroup
AFTER DELETE ON clients
FOR EACH ROW
  BEGIN
    IF (OLD.client_id = 666)
    THEN
      DELETE FROM schedule
      WHERE schedule_id = 666;
    END IF;
  END;
HH1488

DELETE from clients where client_id = 666;